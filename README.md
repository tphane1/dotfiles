# dotfiles

Clone this repository and symlink individual files to their respective directories.

For i3wm config >> ln -s /path/to/git/dotfiles/i3/config ~/.config/i3/config

You can also view the raw format of a file you wish to use and copy/paste into your local copy.
